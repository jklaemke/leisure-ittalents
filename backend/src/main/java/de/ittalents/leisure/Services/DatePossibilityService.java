package de.ittalents.leisure.Services;

import de.ittalents.leisure.Models.Entities.DatePossibility;
import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import de.ittalents.leisure.Models.RestModel.DateService;
import de.ittalents.leisure.Models.RestModel.SimpleDatePossibility;
import de.ittalents.leisure.Repositories.DatePossibilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;

@Service
public class DatePossibilityService {

    @Autowired
    DatePossibilityRepository datePossibilityRepository;
    @Autowired
    ParticipantService participantService;


    public DatePossibility createPossibility(SimpleDatePossibility simpleDatePossibility, EventGroup eventGroup) {
        Date begin = DateService.getDate(simpleDatePossibility.getBegin());
        Date end = null;
        if (simpleDatePossibility.getEnd() != null){
            end = DateService.getDate(simpleDatePossibility.getEnd());
        }else{
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(begin);
            Calendar timecalender = Calendar.getInstance();
            timecalender.setTime(eventGroup.getDuration());
            calendar.add(Calendar.HOUR_OF_DAY, timecalender.get(Calendar.HOUR_OF_DAY));
            calendar.add(Calendar.MINUTE, timecalender.get(Calendar.MINUTE));
            end = new Date(calendar.getTime().getTime());
        }
        Participant participant = participantService.getParticipant(simpleDatePossibility.getParticipant());
        DatePossibility datePossibility = new DatePossibility(begin,end,participant,eventGroup);
        return datePossibility;
    }


    /**
     * Deletes all datepossibilities by eventgroup
     *
     * @param eventGroup
     */
    public void deleteAllDatePossibilitiesByEventGroup(EventGroup eventGroup) {
        for (DatePossibility possibility : eventGroup.getPossibilities()) {
            datePossibilityRepository.delete(possibility);
        }
    }

}
