package de.ittalents.leisure.Services;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import de.ittalents.leisure.Services.Property.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.regex.Pattern;

@Service
public class EmailService {

    @Autowired
    JavaMailSender mailSender;
    @Autowired
    PropertyService propertyService;
    @Autowired
    EventGroupService eventGroupService;


    /**
     * Sends a message
     *
     * @param receiver the receiver email.
     * @param subject  the title of the email
     * @param text     the content of the email
     */
    public void sendEmail(String receiver, String subject, String text) {

        MimeMessage mimeMessage = mailSender.createMimeMessage();

        try {

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            mimeMessageHelper.setSubject(subject);
            mimeMessageHelper.setFrom(propertyService.getEmail_username());
            mimeMessageHelper.setTo(receiver);
            mimeMessageHelper.setText(text);

            mailSender.send(mimeMessageHelper.getMimeMessage());

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends an email to the participant
     * @param participant
     * @param eventGroup
     */
    public void sendEmailWithPid(Participant participant,EventGroup eventGroup ){
        Participant owner = eventGroup.getOwner();
        String message =propertyService.getEmail_text();
        String url = eventGroupService.getURL(eventGroup,participant);
        message = message.replace("%NAME%",String.format("%s %s",participant.getFirstname(),participant.getLastname()));
        message = message.replace("%SENDER%",String.format("%s %s",owner.getFirstname(),owner.getLastname()));
        message = message.replace("%LINK%",url);
        String topic = propertyService.getEmail_topic();
        sendEmail(participant.getEmail(),topic,message);

    }

    public boolean validMail(String email){
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    public void sendEmailFromEventGroup(EventGroup eventGroup) {
        for (Participant participant: eventGroup.getParticipants()){
            if (participant != eventGroup.getOwner()){
                sendEmailWithPid(participant,eventGroup);
            }
        }
    }

    public void sendLastMail(String date, Participant participant, EventGroup event){
        String email = "Hi"+participant.getFirstname() + " "+ participant.getLastname()+" es wurde sich für "+date+" entschieden. ("+eventGroupService.getURL(event,participant)+")";
        sendEmail(participant.getEmail(),"Es wurde entschieden"+event.getTopic(),email);
    }
}
