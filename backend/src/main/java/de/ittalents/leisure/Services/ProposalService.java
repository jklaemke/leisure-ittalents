package de.ittalents.leisure.Services;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import de.ittalents.leisure.Models.Entities.Proposal;
import de.ittalents.leisure.Models.RestModel.SimpleProposal;
import de.ittalents.leisure.Repositories.ProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProposalService {
    @Autowired
    ProposalRepository proposalRepository;

    public void deleteAllProposals(EventGroup eventGroup){
        for (Proposal proposal: eventGroup.getProposals()) {
            proposalRepository.delete(proposal);
        }
    }

    public Proposal createProposal(SimpleProposal simpleProposal, Participant author,EventGroup eventGroup){
        Proposal proposal = new Proposal(simpleProposal.getTitle(),author, eventGroup);
        proposalRepository.save(proposal);
        return proposal;
    }

}
