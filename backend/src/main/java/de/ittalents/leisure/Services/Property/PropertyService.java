package de.ittalents.leisure.Services.Property;

import de.ittalents.leisure.Models.Entities.Property;
import de.ittalents.leisure.Repositories.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@PropertySource("classpath:application.properties")
public class PropertyService {

    @Autowired
    private ConfigurableEnvironment myEnv;
    @Autowired
    PropertyRepository propertyRepository;
    private String[] forbiddenKeys = new String[]{
                "spring.resources.static-locations" ,
                "spring.datasource.url" ,
                "spring.datasource.driverClassName",
                "spring.datasource.username" ,
                "spring.datasource.password" ,
                "spring.jpa.database-platform" ,
                "server.port",
                "spring.jpa.hibernate.ddl-auto",
                "de.leisure.admin.username" ,
                "de.leisure.admin.password",
                "de.leisure.getpath.event" ,
                "de.leisure.getpath.event.param.id"
    };

    @Value("${spring.mail.host}")
    private String email_host;
    @Value("${spring.mail.port}")
    private int email_port;
    @Value("${spring.mail.username}")
    private String email_username;
    @Value("${spring.mail.password}")
    private String email_password;
    @Value("${de.leisure.emailtext}")
    private String email_text;
    @Value("${de.leisure.emailtopic}")
    private String email_topic;

    @Value("${leisure.mail.endtext")
    private String endtext;
    /**
     * Returns special Propertys
     *
     * @param key the key of the property
     * @return
     */
    public String getProperty(String key) {
        Property property = propertyRepository.findByKey(key);
        return property != null ? property.getValue() : null;
    }

    public void updateorInsertProperty(String key, String value) {
        Property property = propertyRepository.findByKey(key);
        if (property != null) {
            propertyRepository.delete(property);
        }
        propertyRepository.save(new Property(key, value));

    }

    public List<Property> getPublicProperties(){
        List<Property> result = new ArrayList<>();
        for (org.springframework.core.env.PropertySource<?> propertySource: myEnv.getPropertySources()) {

            if (propertySource instanceof ResourcePropertySource){
                Properties properties = (Properties) propertySource.getSource();
                MAPITERATION:
                for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                    String key = String.valueOf(entry.getKey()).toLowerCase();
                    for (String fkey : forbiddenKeys) {
                        if (key.equals(fkey)){
                            continue MAPITERATION;
                        }
                    }
                    String value = String.valueOf(entry.getValue());
                    result.add(new Property(key,value));
                }
            }
        }
        for (Property property: result) {
            Property find = propertyRepository.findByKey(property.getKey());
            if (find != null){
                int index = result.indexOf(property);
                result.remove(property);
                result.set(index,find);
            }
        }

        return result;
    }



    public String getEmail_text() {
        String db_email_text = getProperty(PropertyType.MAIL_TEXT);
        return db_email_text != null ? db_email_text : email_text;
    }
    public String getEmail_topic() {
        String db_email_TOPIC = getProperty(PropertyType.MAIL_TOPIC);
        return db_email_TOPIC != null ? db_email_TOPIC : email_topic;
    }

    public String getEmail_host() {
        String db_email_host = getProperty(PropertyType.MAIL_HOST);
        return db_email_host != null ? db_email_host : email_host;
    }

    public int getEmail_port() {
        String db_email_port = getProperty(PropertyType.MAIL_PORT);
        return db_email_port != null ? Integer.parseInt(db_email_port) : email_port;

    }

    public String getEmail_username() {
        String db_email_username = getProperty(PropertyType.MAIL_USERNAME);
        return db_email_username != null ? db_email_username : email_username;
    }

    public String getEmail_password() {
        String db_email_password = getProperty(PropertyType.MAIL_PASSWORD);
        return db_email_password != null ? db_email_password : email_password;
    }



}
