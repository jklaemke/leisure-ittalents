package de.ittalents.leisure.Services;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import de.ittalents.leisure.Models.RestModel.SimpleParticipant;
import de.ittalents.leisure.Repositories.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ParticipantService {

    @Autowired
    ParticipantRepository participantRepository;
    @Autowired
    EmailService emailService;
    private java.util.logging.Logger log = java.util.logging.Logger.getLogger(this.getClass().getName());

    public Participant getOrCreateParticipant(SimpleParticipant simpleParticipant) {
        Participant participant = null;
        if (simpleParticipant.getPid() == null) {
            return createParticipant(simpleParticipant);
        } else {
            return getParticipant(simpleParticipant);
        }
    }

    public Participant getParticipant(SimpleParticipant simpleParticipant) {
        return getParticipant(simpleParticipant.getPid());
    }

    public Participant getParticipant(String pid) {
        return participantRepository.getByPid(pid);
    }

    public Participant createParticipant(SimpleParticipant simpleParticipant) {
        Participant participant = new Participant(simpleParticipant.getEmail(), simpleParticipant.getFirstname(), simpleParticipant.getLastname());
        participantRepository.save(participant);
        return participant;
    }

    public void deleteEventGroupFromAllParticipants(EventGroup eventGroup) {
        List<Participant> participants = participantRepository.getAllByEventGroups(eventGroup);
        for (Participant participant : participants) {
            participant.removeEventGroup(eventGroup);
            if (participant.getEventGroups().size() == 0) {
                participantRepository.delete(participant);
            } else {
                updateParticipant(participant);
            }
        }
    }

    @Transactional
    public void updateParticipant(Participant participant) {
        participantRepository.deleteByPid(participant.getPid());
        participantRepository.save(participant);
    }


}
