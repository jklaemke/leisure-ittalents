package de.ittalents.leisure.Services;


import de.ittalents.leisure.Configuration.CronConfig;
import de.ittalents.leisure.Models.Entities.DatePossibility;
import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import de.ittalents.leisure.Models.Entities.Proposal;
import de.ittalents.leisure.Models.RestModel.DateService;
import de.ittalents.leisure.Models.RestModel.SimpleEventGroup;
import de.ittalents.leisure.Repositories.EventGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.util.*;

@Service
public class EventGroupService {

    @Autowired
    EventGroupRepository eventGroupRepository;
    @Autowired
    ParticipantService participantService;
    @Autowired
    ProposalService proposalService;
    @Autowired
    DatePossibilityService datePossibilityService;
    @Autowired
    EmailService emailService;
    @Autowired
    CronConfig cronConfig;
    @Value("${de.leisure.domain}")
    String domain;
    private java.util.logging.Logger log = java.util.logging.Logger.getLogger(this.getClass().getName());

    public String getURL(EventGroup eventGroup, Participant participant){
        return domain+"/leisure?id"+eventGroup.getGroupId()+"&user="+ participant.getPid();
    }


    public EventGroup getOrCreateEventGroup(SimpleEventGroup simpleEventGroup, Participant owner) {
        if (simpleEventGroup.getGroupid() != null) {
            return getEventGroup(simpleEventGroup);
        } else {
            return createEventGroup(simpleEventGroup, owner);
        }
    }

    public EventGroup getEventGroup(SimpleEventGroup simpleEventGroup) {
        return getEventGroup(simpleEventGroup.getGroupid());
    }

    public EventGroup getEventGroup(String groupid) {
        return eventGroupRepository.getByGroupId(groupid);
    }

    public EventGroup deleteEventGroup(String groupid) {
        EventGroup eventGroup = eventGroupRepository.getByGroupId(groupid);
        if (eventGroup == null) {
            return null;
        }
        participantService.deleteEventGroupFromAllParticipants(eventGroup);
        datePossibilityService.deleteAllDatePossibilitiesByEventGroup(eventGroup);
        proposalService.deleteAllProposals(eventGroup);
        eventGroupRepository.delete(eventGroup);
        return eventGroup;
    }

    public int getMillisecondsToExpireDate(String groupid) {
        Date expire = eventGroupRepository.getByGroupId(groupid).getExpireDate();
        return (int) (expire.getTime() - System.currentTimeMillis());
    }

    public EventGroup createEventGroup(SimpleEventGroup simpleEventGroup, Participant owner) {
        Date begin = DateService.getDate(simpleEventGroup.getBegin());
        Date end = DateService.getDate(simpleEventGroup.getEnd());
        Date deadline = DateService.getDate(simpleEventGroup.getDeadline());
        String topic = simpleEventGroup.getTopic();
        String description = simpleEventGroup.getDescription();
        Date expireDate = cronConfig.getExpire(end);
        EventGroup eventGroup = new EventGroup(owner, begin, end, deadline, topic, description, expireDate);
        if (simpleEventGroup.getDuration() != null) {
            eventGroup.setDuration(DateService.getTime(simpleEventGroup.getDuration()));
        }
        eventGroupRepository.save(eventGroup);
        return eventGroup;
    }

    public EventGroup addParticipants(EventGroup eventGroup, List<Participant> participants) {
        for (Participant participant : participants) {
            participant.addEventGroup(eventGroup);
            eventGroup.addParticipant(participant);
            if (eventGroup.getOwner() != participant) {
                emailService.sendEmailWithPid(participant, eventGroup);
            }
        }
        this.updateEventGroup(eventGroup);
        return eventGroup;
    }

    public List<EventGroup> getAllEventGroupsWithParticipant(Participant participant) {
        return eventGroupRepository.getAllByParticipantsContains(participant);
    }

    public void updateEventGroup(EventGroup eventGroup) {
        //eventGroupRepository.updateEventGroup(eventGroup.getGroupId(),eventGroup.getTopic(),eventGroup.getDescription(),eventGroup.getBegin(),eventGroup.getEnd(),eventGroup.getExpireDate(),eventGroup.getDeadline(),eventGroup.getDuration());
        eventGroupRepository.save(eventGroup);
    }

    /**
     * Searched for expired Events
     */
    @Scheduled(fixedRate = 60000)
    public void clean() {
        searchExpired();
        resolve();
    }

    /**
     * Resolve Dates
     */
    public void resolve() {
        Date now = new Date(System.currentTimeMillis());
        for (EventGroup eventGroup : eventGroupRepository.getAllByDeadlineAfter(now)) {
            Proposal bestproposal = null;
            for (Proposal proposal : eventGroup.getProposals()) {
                if (bestproposal == null || proposal.getYesVotes() >bestproposal.getYesVotes()){
                    bestproposal = proposal;
                }
            }
            Map<String,List<DatePossibility>> datePossibilityMap = new HashMap<>();
            for (DatePossibility datePossibility: eventGroup.getPossibilities())
            {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(datePossibility.getBegin());
                String date = String.format("%2d.%2d.%4d",calendar.get(Calendar.DAY_OF_MONTH),calendar.get(Calendar.MONTH),calendar.get(Calendar.YEAR));
                datePossibilityMap.computeIfAbsent(date, k -> new ArrayList<>());
                datePossibilityMap.get(date).add(datePossibility);
            }
            List<DatePossibility> bestDate = null;
            String result = null;
            for (Map.Entry<String, List<DatePossibility>> entry : datePossibilityMap.entrySet()) {
                if (bestDate == null || bestDate.size() < entry.getValue().size()){
                    bestDate = entry.getValue();
                    result = entry.getKey();
                }
            }

            /* Check nearest Date */
            for (Participant participant: eventGroup.getParticipants() ) {
                emailService.sendLastMail(result!= null ? result :"kein Datum",participant,eventGroup);
            }
        }
    }


    public void searchExpired() {
        log.info("Start removing all depricated  Events");
        Date now = new Date(System.currentTimeMillis());
        for (EventGroup eventGroup : eventGroupRepository.getAllByExpireDateAfter(now)) {
            eventGroupRepository.delete(eventGroup);
            log.info("Deleted Event " + eventGroup.getGroupId() + " because of expired date");
        }
        log.info("Finished removing all expired  Event");
    }

    public void addDatePossibility(EventGroup eventGroup, DatePossibility datePossibility) {
        eventGroup.addDatePossibility(datePossibility);
        updateEventGroup(eventGroup);
    }

    public void addProposal(EventGroup eventGroup, Proposal proposal) {
        eventGroup.addProposal(proposal);
        updateEventGroup(eventGroup);
    }
}
