package de.ittalents.leisure.Services.Property;

public class PropertyType {



    private PropertyType() {
    }

    public static final String MAIL_HOST = "spring.mail.host";
    public static final String MAIL_PORT = "spring.mail.port";
    public static final String MAIL_USERNAME = "spring.mail.username";
    public static final String MAIL_PASSWORD = "spring.mail.password";
    public static final String MAIL_TEXT = "de.leisure.emailtext";
    public static final String MAIL_TOPIC = "de.leisure.emailtopic" ;
    public static final String ADMIN_USERNAME = "de.leisure.admin.username";
    public static final String ADMIN_PASSWORD = "de.leisure.admin.password";

}
