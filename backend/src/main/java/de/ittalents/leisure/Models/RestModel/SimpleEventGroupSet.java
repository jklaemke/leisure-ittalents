package de.ittalents.leisure.Models.RestModel;

import java.util.List;

public class SimpleEventGroupSet {
    private SimpleParticipant owner;
    private SimpleEventGroup event;
    private List<SimpleParticipant> participants;

    public SimpleParticipant getOwner() {
        return owner;
    }

    public void setOwner(SimpleParticipant owner) {
        this.owner = owner;
    }

    public SimpleEventGroup getEvent() {
        return event;
    }

    public void setEvent(SimpleEventGroup event) {
        this.event = event;
    }

    public List<SimpleParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<SimpleParticipant> participants) {
        this.participants = participants;
    }
}
