package de.ittalents.leisure.Models.RestModel;


public class SimpleProposal {


    long id;
    private String title;
    private int yesvotes;
    private SimpleParticipant author;



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYesvotes() {
        return yesvotes;
    }

    public void setYesvotes(int yesvotes) {
        this.yesvotes = yesvotes;
    }

    public SimpleParticipant getAuthor() {
        return author;
    }

    public void setAuthor(SimpleParticipant author) {
        this.author = author;
    }

}
