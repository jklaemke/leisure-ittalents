package de.ittalents.leisure.Models.RestModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateService {
    /**
     * This needs to be a classic singleton because no instance should be on the Simple Restmodels
     It
     */

    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:ss");
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss");
private static SimpleDateFormat germanDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:ss");

    private DateService() {
    }

    /**
     * Converts a date expression to a date
     * @param expression
     * @return
     */
    public static java.sql.Date getDate(String expression) {
        Date result = null;
        try {
            result = dateFormat.parse(expression);
        } catch (ParseException e) {
            try {
                result = germanDateFormat.parse(expression);
            } catch (ParseException parseException) {
                e.printStackTrace();
                parseException.printStackTrace();
                return null;
            }
        }
        return new java.sql.Date(result.getTime());
    }


    /**
     * Converts a date expression to a date
     * @param expression
     * @return
     */
    public static java.sql.Time getTime(String expression) {
        Date result = null;
        try {
            result = timeFormat.parse(expression);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        return new java.sql.Time(result.getTime());
    }
    /**
     * Returns an expression of the date
     * @param date
     * @return
     */
    public static String getExpression(java.sql.Date date) {
        return dateFormat.format(date);
    }

    /**
     * Returns an expression of the date
     * @param time
     * @return
     */
    public static String getExpression(java.sql.Time time) {
        return timeFormat.format(time);
    }

}
