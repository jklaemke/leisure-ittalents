package de.ittalents.leisure.Models.Entities;

import de.ittalents.leisure.Models.RestModel.SimpleProposal;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
public class Proposal {


    @Id
    @GeneratedValue
    long id;
    private String title;
    @ManyToMany
    private Set<Participant> yesVotes;
    @ManyToOne
    private Participant author;
    @ManyToOne
    private EventGroup eventGroup;



    public Proposal(){
        this.yesVotes = new HashSet<>();
    }



    public Proposal(String title,Participant author, EventGroup eventGroup){
        this();
        this.title = title;
        this.author = author;
        this.eventGroup = eventGroup;
    }

    /**
     * Returns all Information about the Proposal as a SimpleProposal
     * @return
     */
    public SimpleProposal getSimpleProposal() {
        SimpleProposal simpleProposal = new SimpleProposal();
        simpleProposal.setId(id);
        simpleProposal.setTitle(title);
        simpleProposal.setYesvotes(yesVotes.size());
        simpleProposal.setAuthor(author.getSimpleParticipant(false));
        return simpleProposal;
    }

    @Override
    public boolean equals(Object obj) {
        String other = ((Proposal)obj).title;
        return other.toLowerCase().equals(this.title.toLowerCase());
    }


    /* GETTER AND SETTER */

    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getYesVotes() {
        return yesVotes.size();
    }

    public Set<Participant> getYesParticipants(){
        return yesVotes;
    }


    public void addYesVote(Participant participant) {
        if (!this.yesVotes.contains(participant)){
            this.yesVotes.add(participant);
        }
    }
    public void removeYesVote(Participant participant) {
        this.yesVotes.remove(participant);
    }

    public Participant getAuthor() {
        return author;
    }

    public void setAuthor(Participant author) {
        this.author = author;
    }

}
