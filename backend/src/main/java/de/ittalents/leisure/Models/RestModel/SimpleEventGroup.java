package de.ittalents.leisure.Models.RestModel;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;

public class SimpleEventGroup {
    private String groupid;
    private String topic;
    private String description;
    private String begin;
    private String end;
    private String duration;
    private String deadline;
    private List<SimpleParticipant> participants;
    private SimpleParticipant owner;
    private List<SimpleProposal> proposals;
    private List<SimpleDatePossibility> possibilities;


    public SimpleEventGroup() {
        this.participants = new ArrayList<>();
        this.proposals = new ArrayList<>();
        this.possibilities = new ArrayList<>();
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SimpleParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<SimpleParticipant> participants) {
        this.participants = participants;
    }

    public void addParticipant(SimpleParticipant simpleParticipant) {
        this.participants.add(simpleParticipant);
    }



    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public SimpleParticipant getOwner() {
        return owner;
    }

    public void setOwner(SimpleParticipant owner) {
        this.owner = owner;
    }

    public List<SimpleProposal> getProposals() {
        return proposals;
    }

    public void setProposals(List<SimpleProposal> proposals) {
        this.proposals = proposals;
    }

    public void addProposal(SimpleProposal proposal) {
        this.proposals.add(proposal);
    }

    public List<SimpleDatePossibility> getPossibilities() {
        return possibilities;
    }

    public void setPossibilities(List<SimpleDatePossibility> possibilities) {
        this.possibilities = possibilities;
    }

    public void addPossibility(SimpleDatePossibility possibility) {
        this.possibilities.add(possibility);
    }


}
