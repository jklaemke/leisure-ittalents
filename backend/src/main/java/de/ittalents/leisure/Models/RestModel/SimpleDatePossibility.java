package de.ittalents.leisure.Models.RestModel;

import de.ittalents.leisure.Models.Entities.DatePossibility;
import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;


public class SimpleDatePossibility {


    private Long id;
    private String begin;
    private String end;
    private SimpleParticipant participant;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public SimpleParticipant getParticipant() {
        return participant;
    }

    public void setParticipant(SimpleParticipant participant) {
        this.participant = participant;
    }
}
