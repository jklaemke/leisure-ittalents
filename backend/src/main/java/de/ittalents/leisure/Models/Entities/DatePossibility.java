package de.ittalents.leisure.Models.Entities;

import de.ittalents.leisure.Models.RestModel.DateService;
import de.ittalents.leisure.Models.RestModel.SimpleDatePossibility;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class DatePossibility {

    @Id
    @GeneratedValue
    private long id;
    private Date begin;
    private Date end;
    @ManyToOne
    private Participant participant;
    @ManyToOne
    private EventGroup eventGroup;


    public DatePossibility(){}

    public DatePossibility(Date begin, Date end, Participant participant, EventGroup eventGroup){
        this.begin = begin;
        this.end = end;
        this.participant = participant;
        this.eventGroup = eventGroup;
    }


    /**
     * Returns a Map of the values which are public.
     * @return a map which became a json object when you send it
     */
    public SimpleDatePossibility getSimpleDatePossibility() {
        SimpleDatePossibility simpleDatePossibility = new SimpleDatePossibility();
        simpleDatePossibility.setId(id);
        simpleDatePossibility.setBegin(DateService.getExpression(begin));
        simpleDatePossibility.setEnd(DateService.getExpression(end));
        simpleDatePossibility.setParticipant(participant.getSimpleParticipant(false));
        return simpleDatePossibility;
    }

    /* GETTER AND SETTER */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public EventGroup getEventGroup() {
        return eventGroup;
    }

    public void setEventGroup(EventGroup eventGroup) {
        this.eventGroup = eventGroup;
    }
}
