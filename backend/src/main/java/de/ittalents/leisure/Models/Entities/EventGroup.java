package de.ittalents.leisure.Models.Entities;


import de.ittalents.leisure.Models.RestModel.DateService;
import de.ittalents.leisure.Models.RestModel.SimpleEventGroup;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Entity
public class EventGroup implements Serializable {

    @Id
    private String groupId;
    private String topic;
    private String description;
    private Date begin;
    private Date end;
    private Date expireDate;
    private Date deadline;
    private Time duration;
    @OneToOne
    private Participant owner;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Proposal> proposals;
    @ManyToMany
    private Set<Participant> participants;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<DatePossibility> possibilities;


    public EventGroup(Participant owner, Date begin, Date end, Date deadline, String topic, String description, Date expireDate) {
        this();
        this.owner = owner;
        this.begin = begin.getTime() < end.getTime() ? begin : end;
        this.end = end.getTime() > begin.getTime() ? end : begin;
        this.duration = new Time(5400000);

        this.deadline = deadline;
        this.participants.add(owner);
        this.topic = topic;
        this.description = description;
        this.expireDate = expireDate;

    }


    public EventGroup() {
        this.participants = new HashSet<>();
        this.possibilities = new HashSet<>();
        this.possibilities = new HashSet<>();
        this.groupId = UUID.randomUUID().toString() + "-" + UUID.randomUUID().toString();
    }

    /**
     * Returns the SimpleEventGroup for an easier rest-transport
     *
     * @return a SimpleEventGroup
     */
    public SimpleEventGroup getSimpleEventGroup() {
        SimpleEventGroup simpleEventGroup = new SimpleEventGroup();
        simpleEventGroup.setTopic(this.topic);
        simpleEventGroup.setDescription(this.description);
        simpleEventGroup.setOwner(this.owner.getSimpleParticipant(false));
        simpleEventGroup.setGroupid(this.groupId);
        simpleEventGroup.setBegin(DateService.getExpression(begin));
        simpleEventGroup.setEnd(DateService.getExpression(end));
        simpleEventGroup.setDuration(DateService.getExpression(duration));
        simpleEventGroup.setDeadline(DateService.getExpression(deadline));
        this.participants.forEach(participant -> simpleEventGroup.addParticipant(participant.getSimpleParticipant(false)));
        this.proposals.forEach(proposal -> simpleEventGroup.addProposal(proposal.getSimpleProposal()));
       this.possibilities.forEach(datePossibility -> simpleEventGroup.addPossibility(datePossibility.getSimpleDatePossibility()));
        return simpleEventGroup;
    }

    public void addDatePossibility(DatePossibility possibility) {
            this.possibilities.add(possibility);
    }

    public boolean isParticipant(String id) {
        for (Participant participant : participants) {
            if (participant.pid.equals(id)) {
                return true;
            }
        }
        return false;
    }

    public void addProposal(Proposal proposal) {
        if (!this.proposals.contains(proposal)) {
            this.proposals.add(proposal);
        }
    }

    public void addParticipant(Participant participant) {
        if (!this.participants.contains(participant)) {
            this.participants.add(participant);
        }
    }

    /* GETTER AND SETTER */

    public String getGroupId() {
        return groupId;
    }


    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Participant getOwner() {
        return owner;
    }

    public void setOwner(Participant owner) {
        this.owner = owner;
    }

    public Set<Proposal> getProposals() {
        return proposals;
    }


    public Set<Participant> getParticipants() {
        return participants;
    }


    public Set<DatePossibility> getPossibilities() {
        return possibilities;
    }

    public void addParticipants(List<Participant> participants) {
        this.participants.addAll(participants);
    }

    public void setDuration(Time duration) {
        this.duration = duration;
    }

    public Time getDuration() {
        return duration;
    }
}
