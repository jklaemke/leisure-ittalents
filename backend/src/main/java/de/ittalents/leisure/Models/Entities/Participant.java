package de.ittalents.leisure.Models.Entities;

import de.ittalents.leisure.Models.RestModel.SimpleParticipant;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;

@Entity
public class Participant {
    @Id
    String pid;
    @NotNull
    private String email;
    private String firstname;
    private String lastname;
    @ManyToMany
    private Set<EventGroup> eventGroups;
    @OneToMany
    private Set<DatePossibility> datePossibilities;

    /**
     * Creates a registered user
     *
     * @param email     the emailadress of the participant
     * @param firstname the firstname of the participant
     * @param lastname  the lastname of the participant
     */
    public Participant(String email, String firstname, String lastname) {
        this();
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public Participant() {
        this.eventGroups = new HashSet<>();
        this.pid = "";
        for (int i = 0; i < 3; i++) {
            pid = pid + UUID.randomUUID().toString().replace("-", "");
        }
    }


    public void addEventGroup(EventGroup eventGroup){
        if (!this.eventGroups.contains(eventGroup)){
            this.eventGroups.add(eventGroup);
        }
    }

    public void removeEventGroup(EventGroup eventGroup) {
        this.eventGroups.remove(eventGroup);
    }


    /**
     * Returns all Information about the User as a Map
     *
     * @return
     */
    public SimpleParticipant getSimpleParticipant(boolean showPid) {
        SimpleParticipant simpleParticipant = new SimpleParticipant();
        simpleParticipant.setEmail(this.email);
        simpleParticipant.setFirstname(this.firstname);
        simpleParticipant.setLastname(this.lastname);
        if (showPid){
            simpleParticipant.setPid(this.pid);
        }
        return simpleParticipant;
    }

    /* GETTER AND SETTER */

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Set<EventGroup> getEventGroups() {
        return eventGroups;
    }

    public void setEventGroups(Set<EventGroup> eventGroups) {
        this.eventGroups = eventGroups;
    }

    public Set<DatePossibility> getDatePossibilities() {
        return datePossibilities;
    }

    public void setDatePossibilities(Set<DatePossibility> datePossibilities) {
        this.datePossibilities = datePossibilities;
    }

    @Override
    public boolean equals(Object obj) {
        return ((Participant) obj).email.toLowerCase().equals(this.email.toLowerCase());
    }
}