package de.ittalents.leisure;

import de.ittalents.leisure.Services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.Entity;

@SpringBootApplication
public class LeisureApplication implements CommandLineRunner {

	@Value("${server.port}")
	private int port;
	private String url;
	private java.util.logging.Logger log = java.util.logging.Logger.getLogger(LeisureApplication.class.getName());

	@Autowired
	EmailService emailService;
	public static void main(String[] args) {
		SpringApplication.run(LeisureApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		url = "http://localhost:"+String.valueOf(port)+"/";
		log.info("System started. Go to "+url);
		log.info("For Configuration go to "+url + "config");
	}


}
