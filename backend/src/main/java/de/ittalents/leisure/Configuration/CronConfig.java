package de.ittalents.leisure.Configuration;

import de.ittalents.leisure.Exceptions.NotValidCronExpression;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableScheduling
public class CronConfig {

    @Value("${de.leisure.expire.cron}")
    private String depricateTime;
    private int minute;
    private int hour;
    private int day;
    private int month;
    private int year;
    private boolean init = false;

    public void initialize() {
        if (!init) {
            init = true;
            try {
                String[] cronsplit = depricateTime.split(" ");
                minute = Integer.parseInt(cronsplit[0]);
                hour = Integer.parseInt(cronsplit[1]);
                day = Integer.parseInt(cronsplit[2]);
                month = Integer.parseInt(cronsplit[3]);
                year = Integer.parseInt(cronsplit[4]);
            } catch (ArrayIndexOutOfBoundsException e) {

            } catch (NumberFormatException e) {
                throw new NotValidCronExpression(depricateTime);
            }
        }
    }

    /**
     * Creates the next ExpireDate from this date
     *
     * @param date the begin date
     * @return the expire Date
     */
    public java.sql.Date getExpire(java.sql.Date date) {
        Date convert = new Date(date.getTime());
        Date result = getExpire(convert);
        return new java.sql.Date(result.getTime());
    }


    /**
     * Creates the next ExpireDate from this date
     *
     * @param date the begin date
     * @return the expire Date
     */
    public Date getExpire(Date date) {
        initialize();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minute);
        calendar.add(Calendar.HOUR_OF_DAY, hour);
        calendar.add(Calendar.DATE, day);
        calendar.add(Calendar.MONTH, month);
        calendar.add(Calendar.YEAR, year);
        return calendar.getTime();
    }


}
