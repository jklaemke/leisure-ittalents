package de.ittalents.leisure.Exceptions.MailExceptions;

public abstract class EmailException extends RuntimeException {

    private String publicInfo;

    public String getPublicInfo() {
        return publicInfo;
    }
    public EmailException(String message){
        this(message,message);
    }


    public EmailException(String publicInfo, String serverinfo){
        super(serverinfo);
        this.publicInfo = publicInfo;
    }


}
