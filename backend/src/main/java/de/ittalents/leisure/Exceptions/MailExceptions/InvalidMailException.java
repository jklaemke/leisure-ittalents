package de.ittalents.leisure.Exceptions.MailExceptions;

public class InvalidMailException extends EmailException {

    public InvalidMailException(String email) {
        super(String.format("The emailadress \"%s\" is not valid",email));
    }
}
