package de.ittalents.leisure.Exceptions;

public class NotValidCronExpression extends RuntimeException{

    public NotValidCronExpression(String expression){
        super(String.format("This expression (%s) is wrong. Need to be \"* * * *\". \n"+
                "(minutes hours days months) \nthese values should be at least 0",expression));
    }
}
