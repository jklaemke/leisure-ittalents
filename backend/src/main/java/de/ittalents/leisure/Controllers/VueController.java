package de.ittalents.leisure.Controllers;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Services.EventGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class VueController {

    @Autowired
    EventGroupService eventGroupService;

    @RequestMapping(value = "/leisure", method = RequestMethod.GET)
    public void redirectIndex(@RequestParam String id, @RequestParam String user, HttpServletResponse resp) throws IOException {
        EventGroup eventGroup = eventGroupService.getEventGroup(id);
        if (eventGroup != null ) {
            Cookie eventid = new Cookie("event", id);
            Cookie userid = new Cookie("user", user);
           /* eventid.setHttpOnly(true);
            userid.setHttpOnly(true);*/
            int expire = (int) (eventGroup.getExpireDate().getTime() - System.currentTimeMillis());
            eventid.setMaxAge(expire);
            userid.setMaxAge(expire);
            eventid.setPath("/");
            userid.setPath("/");
            resp.addCookie(eventid);
            resp.addCookie(userid);
        }
        resp.sendRedirect("/");
    }


}
