package de.ittalents.leisure.Controllers;

        import de.ittalents.leisure.Exceptions.MailExceptions.EmailException;
        import de.ittalents.leisure.Models.Entities.EventGroup;
        import de.ittalents.leisure.Models.Entities.Participant;
        import de.ittalents.leisure.Models.RestModel.*;
        import de.ittalents.leisure.Services.EmailService;
        import de.ittalents.leisure.Services.EventGroupService;
        import de.ittalents.leisure.Services.ParticipantService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.http.HttpStatus;
        import org.springframework.http.MediaType;
        import org.springframework.http.ResponseEntity;
        import org.springframework.mail.MailException;
        import org.springframework.web.bind.annotation.*;

        import javax.mail.Part;
        import java.sql.Date;
        import java.util.*;

        import static org.springframework.web.bind.annotation.RequestMethod.POST;
        import static org.springframework.web.bind.annotation.RequestMethod.PUT;


@RestController()
@RequestMapping("/api/eventset/")
public class EventSetController {


    @Autowired
    EventGroupService eventGroupService;
    @Autowired
    ParticipantService participantService;


    @RequestMapping(value = "/", method = POST)
    @ResponseBody
    public Map<String,Object> createEventwithNoJSON(@RequestBody SimpleEventGroupSet eventGroupSet){
        return null;
    }



    /**
     * Create
     * @param eventGroupSet
     * @return
     */
    @RequestMapping(value = "/", method = POST,produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String,Object> createEvent(@RequestBody SimpleEventGroupSet eventGroupSet){
        Participant owner = participantService.getOrCreateParticipant(eventGroupSet.getOwner());
        SimpleEventGroup simpleEventGroup = eventGroupSet.getEvent();
        EventGroup eventGroup = eventGroupService.getOrCreateEventGroup(simpleEventGroup,owner);
        List<Participant> participants = new ArrayList<>();
        for (SimpleParticipant simpleParticipant: eventGroupSet.getParticipants()) {
           participants.add(participantService.getOrCreateParticipant(simpleParticipant));
        }
        eventGroupService.addParticipants(eventGroup,participants);

        Map<String,Object> result = new HashMap<>();

        result.put("successful",true);
        result.put("ownerid",owner.getPid());
        result.put("groupid",eventGroup.getGroupId());
        return result;
    }




}
