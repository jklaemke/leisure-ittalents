package de.ittalents.leisure.Controllers;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.RestModel.SimpleEventGroup;
import de.ittalents.leisure.Services.EventGroupService;
import de.ittalents.leisure.Services.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;


@RestController()
@RequestMapping("/api/event")
public class EventController {


    @Autowired
    EventGroupService eventGroupService;
    @Autowired
    ParticipantService participantService;


    /**
     * Create a new EventGroup from a simpleEventGroup
     *
     * @param simpleEventGroup
     * @return the simpleEventGroup with all information
     */
    @RequestMapping(value = "/", method = POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SimpleEventGroup createEvent(@RequestBody SimpleEventGroup simpleEventGroup) {
        return null;
    }

    /**
     * Update a new EventGroup from a simpleEventGroup
     *
     * @param simpleEventGroup
     * @return the simpleEventGroup with all information
     */
    @RequestMapping(value = "/", method = PUT, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SimpleEventGroup updateEvent(@RequestBody SimpleEventGroup simpleEventGroup) {
        return null;
    }

    /**
     * Update a new EventGroup from a simpleEventGroup
     *
     * @return the simpleEventGroup with all information
     */

    @RequestMapping(value = "/{id}", method = GET)
    @ResponseBody
    public SimpleEventGroup getEvent(@PathVariable("id") String id) {
        EventGroup eventGroup = eventGroupService.getEventGroup(id);
        return eventGroup == null ? new SimpleEventGroup() : eventGroup.getSimpleEventGroup();
    }


    /**
     * Remove an EventGroup
     *
     * @return the simpleEventGroup with all information
     */
    @RequestMapping(value = "/event/{id}", method = DELETE, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> deleteEvent(@PathVariable("id") String id) {
        EventGroup delete = eventGroupService.deleteEventGroup(id);
        Map<String, Object> result = new HashMap<>();
        result.put("successful deleted", delete != null);
        result.put("event", delete != null ? delete.getGroupId() : null);
        return result;
    }


}
