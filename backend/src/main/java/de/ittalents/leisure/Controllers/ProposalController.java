package de.ittalents.leisure.Controllers;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import de.ittalents.leisure.Models.Entities.Proposal;
import de.ittalents.leisure.Models.RestModel.SimpleProposal;
import de.ittalents.leisure.Services.EventGroupService;
import de.ittalents.leisure.Services.ParticipantService;
import de.ittalents.leisure.Services.ProposalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class ProposalController {

    @Autowired
    ProposalService proposalService;
    @Autowired
    EventGroupService eventGroupService;
    @Autowired
    ParticipantService participantService;

    @MessageMapping("/proposal/{eventid}")
    @SendTo("/topic/proposal/{eventid}")
    public SimpleProposal proposal(@DestinationVariable String eventid, SimpleProposal simpleProposal) throws Exception {
        //TODO: Logic
        Participant author = participantService.getParticipant(simpleProposal.getAuthor());
        EventGroup eventGroup = eventGroupService.getEventGroup(eventid);
        Proposal proposal = proposalService.createProposal(simpleProposal, author,eventGroup);
        eventGroupService.addProposal(eventGroup, proposal);
        return proposal.getSimpleProposal();



    }


}
