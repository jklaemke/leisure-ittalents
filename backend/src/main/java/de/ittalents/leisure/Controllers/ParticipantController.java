package de.ittalents.leisure.Controllers;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import de.ittalents.leisure.Models.RestModel.SimpleEventGroup;
import de.ittalents.leisure.Models.RestModel.SimpleParticipant;
import de.ittalents.leisure.Services.EventGroupService;
import de.ittalents.leisure.Services.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController()
@RequestMapping("/api/participant")
public class ParticipantController {

    @Autowired
    ParticipantService participantService;
    @Autowired
    EventGroupService eventGroupService;


    @RequestMapping(value = "/{pid}", method = GET)
    @ResponseBody
    public SimpleParticipant getParticipant(@PathVariable("pid") String pid){
       Participant participant = participantService.getParticipant(pid);
        return participant == null ? new SimpleParticipant(): participant.getSimpleParticipant(true);
    }
}
