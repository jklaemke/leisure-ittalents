package de.ittalents.leisure.Controllers;

import de.ittalents.leisure.Models.Entities.DatePossibility;
import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import de.ittalents.leisure.Models.Entities.Proposal;
import de.ittalents.leisure.Models.RestModel.SimpleDatePossibility;
import de.ittalents.leisure.Models.RestModel.SimpleProposal;
import de.ittalents.leisure.Services.DatePossibilityService;
import de.ittalents.leisure.Services.EventGroupService;
import de.ittalents.leisure.Services.ParticipantService;
import de.ittalents.leisure.Services.ProposalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class DatePossibilityController {


    @Autowired
    EventGroupService eventGroupService;
    @Autowired
    DatePossibilityService datePossibilityService;

    @MessageMapping("/datepossibility/{eventid}")
    @SendTo("/topic/datepossibility/{eventid}")
    public SimpleDatePossibility proposal(@DestinationVariable String eventid, SimpleDatePossibility simpleDatePossibility) throws Exception {
        EventGroup eventGroup = eventGroupService.getEventGroup(eventid);
        DatePossibility datePossibility = datePossibilityService.createPossibility(simpleDatePossibility, eventGroup);
        eventGroupService.addDatePossibility(eventGroup,datePossibility);
        SimpleDatePossibility sd = datePossibility.getSimpleDatePossibility();
        return sd;


    }


}
