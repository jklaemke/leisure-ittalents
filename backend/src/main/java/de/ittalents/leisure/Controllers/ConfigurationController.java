package de.ittalents.leisure.Controllers;

import de.ittalents.leisure.Models.Entities.Property;
import de.ittalents.leisure.Services.EmailService;
import de.ittalents.leisure.Services.Property.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Tuple;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ConfigurationController {

    @Autowired
    PropertyService propertyService;
    @Autowired
    EmailService emailService;


    @Secured("ROLE_ADMIN")
    @GetMapping("/config")
    public String config(Model model) {

        return showConfigPage(model);
    }

    //consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(
            path = "/config"
    )
    public String changedConfig(Model model,HttpServletRequest request) throws Exception {

        if ( request.getParameter("receiver") != null){
            return sendTestMail(request.getParameter("receiver"),model);
        }

        String host = getStringParameter(request, "host", propertyService.getEmail_host());
        int port = getIntegerParameter(request, "port", propertyService.getEmail_port());
        String username = getStringParameter(request, "username", propertyService.getEmail_host());
        String password = getStringParameter(request, "password", propertyService.getEmail_password());
        String mailtext = getStringParameter(request, "mailtext", propertyService.getEmail_password());

        propertyService.updateorInsertProperty(PropertyType.MAIL_HOST, host);
        propertyService.updateorInsertProperty(PropertyType.MAIL_PORT, String.valueOf(port));
        propertyService.updateorInsertProperty(PropertyType.MAIL_USERNAME, username);
        propertyService.updateorInsertProperty(PropertyType.MAIL_PASSWORD, password);
        propertyService.updateorInsertProperty(PropertyType.MAIL_TEXT, String.valueOf(mailtext));
        return showConfigPage(model);
    }

    private String showConfigPage(Model model){
        List<Property> properties = propertyService.getPublicProperties();
        model.addAttribute("properties",properties);
        return "Configuration.html";
    }


    public String sendTestMail(String receiver, Model model) throws Exception {

        //TODO: Anpassen der Nachricht
        emailService.sendEmail(receiver,"Test","Das ist ein Test");
        return showConfigPage(model);
    }


    String getStringParameter(HttpServletRequest request, String parameter, String defaultValue) {
        return (String) getParameter(request, parameter, defaultValue);
    }

    boolean getBooleanParameter(HttpServletRequest request, String parameter, boolean defaultValue) {
        return (boolean) getParameter(request, parameter, defaultValue);
    }

    int getIntegerParameter(HttpServletRequest request, String parameter, int defaultValue) {
        return (int) getParameter(request, parameter, defaultValue);
    }

    Object getParameter(HttpServletRequest request, String parameter, Object defaultValue) {
        String value = request.getParameter(parameter);
        if (value != null) {
            if (defaultValue instanceof String) {
                return value;
            }
            if (defaultValue instanceof Boolean) {
                return Boolean.valueOf(value);
            }
            if (defaultValue instanceof Integer) {
                return Integer.parseInt(value);
            }
        }
        return defaultValue;

    }


}
