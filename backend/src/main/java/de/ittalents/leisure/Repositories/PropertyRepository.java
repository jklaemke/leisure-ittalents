package de.ittalents.leisure.Repositories;

import de.ittalents.leisure.Models.Entities.Property;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyRepository extends JpaRepository<Property,String> {

    Property findByKey(String key);

}
