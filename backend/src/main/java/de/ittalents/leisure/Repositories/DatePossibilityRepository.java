package de.ittalents.leisure.Repositories;

import de.ittalents.leisure.Models.Entities.DatePossibility;
import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Property;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface DatePossibilityRepository extends JpaRepository<DatePossibility,String> {

    DatePossibility getById(long id);


}
