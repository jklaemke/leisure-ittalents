package de.ittalents.leisure.Repositories;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Property;
import de.ittalents.leisure.Models.Entities.Proposal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProposalRepository extends JpaRepository<Proposal,String> {

    Proposal findById(long id);



}
