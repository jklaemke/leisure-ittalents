package de.ittalents.leisure.Repositories;

import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Part;
import java.util.List;

public interface ParticipantRepository extends JpaRepository<Participant,String> {
    @Transactional
    Participant getByEmail(String email);
    @Transactional
    Participant getByPid(String pid);
    @Transactional
    public void deleteByPid(String pid);
    @Transactional
    List<Participant> getAllByEventGroups(EventGroup eventgroup);
}
