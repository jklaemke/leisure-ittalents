package de.ittalents.leisure.Repositories;


import de.ittalents.leisure.Models.Entities.EventGroup;
import de.ittalents.leisure.Models.Entities.Participant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Time;
import java.util.List;
import java.util.Optional;

public interface EventGroupRepository extends JpaRepository<EventGroup,String> {

    EventGroup getByGroupId(String groupid);



    @Modifying
    @Transactional
    @Query("update EventGroup eg set eg.topic = ?2, eg.description =?3, eg.begin=?4, eg.end=?5,eg.expireDate=?6, eg.deadline = ?7,eg.duration=?8 where eg.groupId = ?1")
    void updateEventGroup(String groupid, String topic, String description, Date begin, Date end, Date expireDate, Date deadline, Time duration);

    @Transactional
    List<EventGroup> getAllByDeadlineAfter(Date date);

    @Transactional
    List<EventGroup> getAllByExpireDateAfter(Date date);
    @Transactional
    List<EventGroup> getAllByParticipantsContains(Participant participant);



    @Transactional
    void deleteByGroupId(String groupId);

}
