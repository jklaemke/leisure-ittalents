import Vue from 'vue';
import App from './components/App.vue'
import VueCookies from 'vue-cookies'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSession from 'vue-session'

/*

if('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/.../src/serviceworker.js', {scope: '/'})
        .then(registration => { console.log('Service Worker registered... : ', registration);})
        .catch(registrationError => { console.log('Service Worker registration failed: ',registrationError);})
    });
}
Vue.config.devtools = true*/
Vue.use(VueSession,{persist :true})
Vue.use(VueAxios, axios)
Vue.use(VueCookies)
Vue.$cookies.config('session','','',false)
new Vue({
    el: '#app',
    render: h => h(App)
  })
