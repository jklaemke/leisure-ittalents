# Requirements for a Docker Container

## Backend

Maven und Java muss installiert sein.

1. cd to pom.xml and run : `mvn install`
2. under folder \target exist leisure-0.0.1-SNAPSHOT.jar
3. run command `java -jar leisure-0.0.1-SNAPSHOT.jar` on this folder

## Frontend

Node bzw. npm müssen installiert sein.

1. cd to /backend/src/main/resources/static/
2. run command `npm install`
