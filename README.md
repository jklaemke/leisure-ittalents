# Leisure IT Talents

Dies ist die Abgabe von Julian Klämke und Deniz Aydar für [it talents und enowa](https://www.it-talents.de/foerderung/code-competition/code-competition-04-2020)


Wie starte ich das Backend?

Über einen Docker:
Um die Imagefile zu erstellen, gehe ins Verzeichnis, in der die Dockerfile ist. 
Dort rufe:
`docker build -t NAME .`
auf.
NAME mit beliebigen Dockernamen ändern.
Es wird eine Image Datei erstellt..


Nur mit MAVEN:
Rufe den Befehl:
`mvn install` in dem Verzeichnis, in der die POM.xml ist (backend).
Zum starten wechsel ins Verzeichnis `target`
und rufe
`java -jar leisure-0.0.1-SNAPSHOT.jar`


Laufzeitinformation
 Das Frontend zieht sich bereits bei der ersten Installation alle Dependencies und muss nicht weiter beachtet werden.
Java Spring "serviert" das Frontend, welches im static ordner zu finden ist.
Bevor der Server gestartet wird müssen in den application.properties (welche man unter ressources - klassisch Java Spring)  einige Einstellungen geändert werden.
Einige Einstellungen können auch während der Laufzeit über /config geändert werden.
Config ist jedoch geschützt und kann nur mit dem Passwort und Usernamen aus den application.properties aufgerufen werden.

Uns war es wichtig  bei der Webanwendung auf Registrierungen zu verzichten, da diese meist Zeitaufwändig sind.
Daher arbeitet die Anwendung ausschließlich mit Cookies.


###Email
Wichtig ist es in der application.properties
die Emaileinstellungen gemäß Java Spring richtig zu konfigurieren.

