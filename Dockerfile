FROM maven:3-jdk-8 AS build
WORKDIR /build
COPY backend/pom.xml .
RUN mvn dependency:go-offline
COPY backend/src/ /build/src
RUN mvn clean install

FROM openjdk:10.0.1 as entry
COPY --from=build build/target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]